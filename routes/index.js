const express = require("express");
const router = express.Router();

// Models
const { User } = require("../models");

// Login page
router.get("/", (req, res) => {
    res.render("login");
});

// Do login
router.post("/login", (req, res) => {
    const { username, password } = req.body;

    User.findOne({ where: { username, password } })
        .then((user) => {
            if (user) {
                res.redirect("/dashboard");
            } else {
                console.log("User Tidak Ada");
                res.redirect("/");
            }
        })
        .catch((error) => {
            console.log(error);
        });
});

// Dashboard page - list users
router.get("/dashboard", (req, res) => {
    User.findAll({})
        .then((users) => {
            res.render("dashboard", { title: "Dashboard", users });
        })
        .catch((error) => {
            console.log(error);
        });
});

// Add user page
router.get("/dashboard/add", (req, res) => {
    res.render("add", { title: "Add User" });
});

// Do add a user
router.post("/user/add", (req, res) => {
    const { username, password } = req.body;

    User.create({
        username: username,
        password: password,
    })
        .then((user) => {
            res.redirect("/dashboard");
        })
        .catch((error) => {
            console.log(error);
        });
});

// Do delete a user with specific id
router.get("/user/delete/:user_id", (req, res) => {
    const userId = req.params.user_id;

    User.destroy({
        where: { id: userId },
    })
        .then(() => {
            res.redirect("/dashboard");
        })
        .catch((error) => {
            console.log(error);
        });
});

// Update user page
router.get("/dashboard/update/:user_id", (req, res) => {
    const userId = req.params.user_id;

    User.findOne({ where: { id: userId } })
        .then((user) => {
            if (user) {
                res.render("update", { title: "Update User", user: user });
            } else {
                res.redirect("/dashboard");
            }
        })
        .catch((error) => {
            console.log(error);
        });
});

//  Do update a user with specific id
router.post("/user/update/:user_id", (req, res) => {
    const userId = req.params.user_id;
    const { username, password } = req.body;

    User.update(
        {
            username: username,
            password: password,
        },
        {
            where: { id: userId },
        }
    )
        .then((user) => {
            res.redirect("/dashboard");
        })
        .catch((error) => {
            console.log(error);
        });
});

module.exports = router;
