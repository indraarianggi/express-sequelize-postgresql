# Latihan Challenge Binar Academy FSW Chapter 6

## Resources

1. [Sequelize official docs](https://sequelize.org/master/index.html)
2. [Node.js Rest APIs example with Express, Sequelize & MySQL - BezKoder
   ](https://bezkoder.com/node-js-express-sequelize-mysql/)
3. [The Basics Of PostgreSQL UUID Data Type](https://www.postgresqltutorial.com/postgresql-uuid/)
4. Materi Binar Academy FSW Chapter 6
