const express = require("express");
const app = express();

const indexRoutes = require("./routes");

app.use(express.urlencoded({ extended: false }));

app.set("view engine", "ejs");

// setup routes
app.use("/", indexRoutes);

const port = 3000;
app.listen(port, () => console.log(`App listening to port ${port}`));
